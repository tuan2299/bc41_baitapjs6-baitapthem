/**
 * input:tự xét điều kiện nhập vào
 *
 * bước 1: tạo var chẳn và var lẻ
 * bước 2: xét giá trị bé hơn hoặc = 100
 * bước 3: hiển thị các các số chẵn và số lẻ
 *
 * output: hiển thị số chẵn và số lẻ
 */

function ketQua1() {
  var stringChan = "";
  var stringLe = "";

  for (var i = 0; i <= 100; i++) {
    if (i % 2 == 0) {
      stringChan = stringChan + i + " ";
    } else {
      stringLe = stringLe + i + " ";
    }
  }
  document.getElementById("result1").innerText = `👉Số chẵn: ${stringChan}.
                                                  👉Số lẽ: ${stringLe}.`;
}

/**
 * input: tự xét điều kiện nhập vào của người dùng
 *
 * bước 1: tạo một var number
 * bước 2: tạo for if và xét điều kiện
 * bước 3: nếu thoả điều kiện for if thì var sẽ tăng lên 1
 *
 * output: hiển thị các số chia hết cho 3 và nhỏ hơn 1000
 */

function ketQua2() {
  var number = "";
  for (let index = 0; index < 1000; index++) {
    if (index % 3 == 0) {
      number++;
    }
  }
  document.getElementById(
    "result2"
  ).innerText = `👉Số chia hết cho 3 nhỏ hơn 1000: ${number} số.`;
}

/**
 *input: tự xét điều kiện nhập vào

 * bước 1: tạo var số và một var tổng
 * bước 2: xét for if cộng dồn number += index
 * bước 3: nếu đúng điều kiện thì sum = index
 *
 * output: hiển thị số nguyên dương nhỏ nhất
 */

function ketQua3() {
  var number = 0;
  var sum = 0;
  for (var index = 1; index < 10000; index++) {
    if ((number = number + index) && number > 10000) {
      sum = index;
      break;
    }
    document.getElementById(
      "result3"
    ).innerText = `👉Số nguyên dương nhỏ nhất là ${sum}.`;
  }

  /**
   * input: cho người dùng nhập 2 số bất kì
   *
   * bước 1: tạo 2 biến cho 2 numberX và numberY, tạo thêm 1 var sum = 0
   * bước 2: dùng for if
   * bước 3: nếu thoả điều kiện thì sum += Math.pow(numberX, numberY)
   *
   * output: hiển thị tổng
   */

  function ketQua4() {
    var numberX = document.getElementById("txt-number-x").value * 1;
    var numberY = document.getElementById("txt-number-y").value * 1;
    var sum = 0;
    for (let index = 1; index <= numberY; index++) {
      sum += Math.pow(numberX, index);
    }
    document.getElementById("result4").innerText = `👉Tổng: ${sum}.`;
  }
  /**
   * input: cho người dùng nhập 1 số bất kì
   *
   * bước 1: tạo var numberN, let = sum = 1;
   * bước 2: dùng for if
   * bước 3: nếu đúng điều kiện thì sum *= index
   *
   * output: hiển thị giai thừa
   *
   */
  function ketQua5() {
    var numberN = document.getElementById("txt-number5").value * 1;
    let sum = 1;
    for (let index = 1; index <= numberN; index++) {
      sum = sum * index;
    }
    document.getElementById("result5").innerText = `👉Giai thừa: ${sum}.`;
  }
  /**
   * input: tự cho điều kiện
   *
   * bước 1: tạo 1 strinng rỗng
   * bước 2: dùng for if
   * bước 3: xét điều kiện nếu index % 2 == 0 thì là chẵn sẽ tạo 1 div chẵn và người lại
   *
   * output: hiển thị các div chẵn và div lẻ
   */
  function ketQua6() {
    var div = "";
    for (let index = 1; index <= 10; index++) {
      index % 2 == 0
        ? (div = div + `<div id="le" class="bg-danger py-2">👉Div Chẵn</div>`)
        : (div = div + `<div id="le" class="bg-primary py-2">👉Div Lẻ</div>`);
    }
    document.getElementById("result6").innerHTML = div;
  }

  function ketQua7() {
    var number7 = document.getElementById("txt-number7").value * 1;
    var sum = "";
    for (let index = 1; index <= number7; index++) {}
  }
}
